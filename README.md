# README #

1/3スケールのNational/Panasonic CF-2000風小物のstlファイルです。 スイッチ類、I/Oポート等は省略しています。

組み合わせたファイルとパーツごとに配置したファイルの二種類があります。 元ファイルはAUTODESK 123D DESIGNです。

***

# 実機情報

## メーカ
- National
- 松下電器産業

## 発売時期
- 1983年

## 参考資料

- [懐かしのホビーパソコン紹介](https://twitter.com/i/events/888369783116152832?lang=ja)
- [AKIBA PC Hotline!](https://akiba-pc.watch.impress.co.jp/docs/column/retrohard/1074122.html)
- [MSX wiki](https://www.msx.org/wiki/National_CF-2000)

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_cf-2000/raw/b73cb2caf872f03da0e5ae6fb45d42e0ac0a2670/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_cf-2000/raw/b73cb2caf872f03da0e5ae6fb45d42e0ac0a2670/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_cf-2000/raw/b73cb2caf872f03da0e5ae6fb45d42e0ac0a2670/ExampleImage.jpg)
